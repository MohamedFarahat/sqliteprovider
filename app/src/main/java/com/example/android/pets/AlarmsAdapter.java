package com.example.android.pets;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.pets.data.AlarmContract;

/**
 * Created by mohamed on 9/10/2016.
 */
public class AlarmsAdapter extends CursorAdapter {


    public AlarmsAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.alarms_item_list, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView alarmNameTextView = (TextView) view.findViewById(R.id.name);
        TextView alarmDescriptionTextView = (TextView) view.findViewById(R.id.summary);

        // Extract properties from cursor
        int AlarmNameIndex = cursor.getColumnIndex(AlarmContract.AlarmEntry.COLUMN_ALARM_NAME);
        int AlarmDescriptionIndex = cursor.getColumnIndex(AlarmContract.AlarmEntry.COLUMN_ALARM_DESCRIPTION);

        String AlarmName = cursor.getString(AlarmNameIndex);
        String AlarmDescription = cursor.getString(AlarmDescriptionIndex);

        if (TextUtils.isEmpty(AlarmDescription)) {
            AlarmDescription = context.getString(R.string.unknown_desc);
        }

        // Update the TextViews with the attributes for the current alarm
        alarmNameTextView.setText(AlarmName);
        alarmDescriptionTextView.setText(AlarmDescription);
    }
}