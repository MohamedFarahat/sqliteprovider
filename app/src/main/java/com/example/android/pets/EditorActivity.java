/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.pets;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.pets.data.AlarmContract.AlarmEntry;

/**
 * Allows user to create a new Alarm or edit an existing one.
 */
public class EditorActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * Identifier for the Alarm data loader
     */
    private static final int EXISTING_ALARM_LOADER = 0;

    /**
     * Content URI for the existing Alarm (null if it's a new Alarm)
     */
    private Uri mCurrentAlarmUri;

    /**
     * EditText field to enter the Alarm's name
     */
    private EditText mNameEditText;

    /**
     * EditText field to enter the Alarm's description
     */
    private EditText mDescriptionEditText;

    /**
     * EditText field to enter the Alarm's time
     */
    private EditText mTimeEditText;

    /**
     * EditText field to enter the Alarm's gender
     */
    private Spinner mAlarmStatueSpinner;

    /**
     * Gender of the Alarm. The possible valid values are in the AlarmContract.java file:
     * {@link AlarmEntry#ALARM_UNKNOWN}, {@link AlarmEntry#GENDER_ALARM}, or
     * {@link AlarmEntry#GENDER_NO_ALARM}.
     */
    private int mGender = AlarmEntry.ALARM_UNKNOWN;

    private boolean mAlarmHasChanged = false;
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mAlarmHasChanged = true;
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);


        // Examine the intent that was used to launch this activity,
        // in order to figure out if we're creating a new Alarm or editing an existing one.
        Intent intent = getIntent();
        mCurrentAlarmUri = intent.getData();

        // If the intent DOES NOT contain a Alarm content URI, then we know that we are
        // creating a new Alarm.
        if (mCurrentAlarmUri == null) {
            // This is a new Alarm, so change the app bar to say "Add a Alarm"
            setTitle(getString(R.string.editor_activity_title_new_alarm));
            invalidateOptionsMenu();
        } else {
            // Otherwise this is an existing Alarm, so change app bar to say "Edit Alarm"
            setTitle(getString(R.string.editor_activity_title_edit_alarm));

            // Initialize a loader to read the Alarm data from the database
            // and display the current values in the editor
            getLoaderManager().initLoader(EXISTING_ALARM_LOADER, null, this);
        }

        // Find all relevant views that we will need to read user input from
        mNameEditText = (EditText) findViewById(R.id.edit_pet_name);
        mDescriptionEditText = (EditText) findViewById(R.id.edit_pet_breed);
        mTimeEditText = (EditText) findViewById(R.id.edit_pet_weight);
        mAlarmStatueSpinner = (Spinner) findViewById(R.id.spinner_gender);

        setupSpinner();

        mNameEditText.setOnTouchListener(mTouchListener);
        mDescriptionEditText.setOnTouchListener(mTouchListener);
        mTimeEditText.setOnTouchListener(mTouchListener);
        mAlarmStatueSpinner.setOnTouchListener(mTouchListener);


    }

    /**
     * Setup the dropdown spinner that allows the user to select the gender of the Alarm.
     */
    private void setupSpinner() {
        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout
        ArrayAdapter genderSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_gender_options, android.R.layout.simple_spinner_item);

        // Specify dropdown layout style - simple list view with 1 item per line
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // Apply the adapter to the spinner
        mAlarmStatueSpinner.setAdapter(genderSpinnerAdapter);

        // Set the integer mSelected to the constant values
        mAlarmStatueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.gender_alarm))) {
                        mGender = AlarmEntry.GENDER_ALARM;
                    } else if (selection.equals(getString(R.string.gender_no_alarm))) {
                        mGender = AlarmEntry.GENDER_NO_ALARM;
                    } else {
                        mGender = AlarmEntry.ALARM_UNKNOWN;
                    }
                }
            }

            // Because AdapterView is an abstract class, onNothingSelected must be defined
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGender = AlarmEntry.ALARM_UNKNOWN;
            }
        });
    }

    /**
     * Get user input from editor and save Alarm into database.
     */
    private void saveAlarm() {
        // Read from input fields
        // Use trim to eliminate leading or trailing white space
        String nameString = mNameEditText.getText().toString().trim();
        String descString = mDescriptionEditText.getText().toString().trim();
        String timeString = mTimeEditText.getText().toString().trim();

        // Check if this is supposed to be a new Alarm
        // and check if all the fields in the editor are blank
        if (mCurrentAlarmUri == null &&
                TextUtils.isEmpty(nameString) && TextUtils.isEmpty(descString) &&
                TextUtils.isEmpty(timeString) && mGender == AlarmEntry.ALARM_UNKNOWN) {
            // Since no fields were modified, we can return early without creating a new Alarm.
            // No need to create ContentValues and no need to do any ContentProvider operations.
            return;
        }

        // Create a ContentValues object where column names are the keys,
        // and Alarm attributes from the editor are the values.
        ContentValues values = new ContentValues();
        values.put(AlarmEntry.COLUMN_ALARM_NAME, nameString);
        values.put(AlarmEntry.COLUMN_ALARM_DESCRIPTION, descString);
        values.put(AlarmEntry.COLUMN_ALARM_ALARM, mGender);
        // If the time is not provided by the user, don't try to parse the string into an
        // integer value. Use 0 by default.
        int time = 0;
        if (!TextUtils.isEmpty(timeString)) {
            time = Integer.parseInt(timeString);
        }
        values.put(AlarmEntry.COLUMN_ALARM_TIME, time);

        // Determine if this is a new or existing Alarm by checking if mCurrentAlarmUri is null or not
        if (mCurrentAlarmUri == null) {
            // This is a NEW Alarm, so insert a new Alarm into the provider,
            // returning the content URI for the new Alarm.
            Uri newUri = getContentResolver().insert(AlarmEntry.CONTENT_URI, values);

            // Show a toast message depending on whether or not the insertion was successful.
            if (newUri == null) {
                // If the new content URI is null, then there was an error with insertion.
                Toast.makeText(this, getString(R.string.editor_insert_alarm_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the insertion was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_insert_alarm_successful),
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            // Otherwise this is an EXISTING Alarm, so update the Alarm with content URI: mCurrentAlarmUri
            // and pass in the new ContentValues. Pass in null for the selection and selection args
            // because mCurrentAlarmUri will already identify the correct row in the database that
            // we want to modify.
            int rowsAffected = getContentResolver().update(mCurrentAlarmUri, values, null, null);

            // Show a toast message depending on whether or not the update was successful.
            if (rowsAffected == 0) {
                // If no rows were affected, then there was an error with the update.
                Toast.makeText(this, getString(R.string.editor_update_alarm_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the update was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_update_alarm_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void deleteAlarm() {
// Only perform the delete if this is an existing Alarm.
        if (mCurrentAlarmUri != null) {
            // Call the ContentResolver to delete the Alarm at the given content URI.
            // Pass in null for the selection and selection args because the mCurrent AlarmUri
            // content URI already identifies the Alarm that we want.
            int rowsDeleted = getContentResolver().delete(mCurrentAlarmUri, null, null);

            // Show a toast message depending on whether or not the delete was successful.
            if (rowsDeleted == 0) {
                // If no rows were deleted, then there was an error with the delete.
                Toast.makeText(this, getString(R.string.editor_delete_alarm_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the delete was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_delete_alarm_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                // Save Alarm to database
                saveAlarm();
                // Exit activity
                finish();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // If this is a new Alarm, hide the "Delete" menu item.
        if (mCurrentAlarmUri == null) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // Since the editor shows all Alarm attributes, define a projection that contains
        // all columns from the Alarm table
        String[] projection = {
                AlarmEntry._ID,
                AlarmEntry.COLUMN_ALARM_NAME,
                AlarmEntry.COLUMN_ALARM_DESCRIPTION,
                AlarmEntry.COLUMN_ALARM_ALARM,
                AlarmEntry.COLUMN_ALARM_TIME};

        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                mCurrentAlarmUri,         // Query the content URI for the current Alarm
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Bail early if the cursor is null or there is less than 1 row in the cursor
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }

        // Proceed with moving to the first row of the cursor and reading data from it
        // (This should be the only row in the cursor)
        if (cursor.moveToFirst()) {
            // Find the columns of Alarm attributes that we're interested in
            int nameColumnIndex = cursor.getColumnIndex(AlarmEntry.COLUMN_ALARM_NAME);
            int descColumnIndex = cursor.getColumnIndex(AlarmEntry.COLUMN_ALARM_DESCRIPTION);
            int genderColumnIndex = cursor.getColumnIndex(AlarmEntry.COLUMN_ALARM_ALARM);
            int timeColumnIndex = cursor.getColumnIndex(AlarmEntry.COLUMN_ALARM_TIME);

            // Extract out the value from the Cursor for the given column index
            String name = cursor.getString(nameColumnIndex);
            String description = cursor.getString(descColumnIndex);
            int gender = cursor.getInt(genderColumnIndex);
            int time = cursor.getInt(timeColumnIndex);

            // Update the views on the screen with the values from the database
            mNameEditText.setText(name);
            mDescriptionEditText.setText(description);
            mTimeEditText.setText(Integer.toString(time));

            // Gender is a dropdown spinner, so map the constant value from the database
            // into one of the dropdown options (0 is Unknown, 1 is Male, 2 is Female).
            // Then call setSelection() so that option is displayed on screen as the current selection.
            switch (gender) {
                case AlarmEntry.GENDER_ALARM:
                    mAlarmStatueSpinner.setSelection(1);
                    break;
                case AlarmEntry.GENDER_NO_ALARM:
                    mAlarmStatueSpinner.setSelection(2);
                    break;
                default:
                    mAlarmStatueSpinner.setSelection(0);
                    break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // If the loader is invalidated, clear out all the data from the input fields.
        mNameEditText.setText("");
        mDescriptionEditText.setText("");
        mTimeEditText.setText("");
        mAlarmStatueSpinner.setSelection(0); // Select "Unknown" gender
    }

    @Override
    public void onBackPressed() {
        // If the Alarm hasn't changed, continue with handling back button press
        if (!mAlarmHasChanged) {
            super.onBackPressed();
            return;
        }

        // Otherwise if there are unsaved changes, setup a dialog to warn the user.
        // Create a click listener to handle the user confirming that changes should be discarded.
        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // User clicked "Discard" button, close the current activity.
                        finish();
                    }
                };

        // Show dialog that there are unsaved changes
        showUnsavedChangesDialog(discardButtonClickListener);
    }

    //below is the cocde for show pop message
    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Keep editing" button, so dismiss the dialog
                // and continue editing the Alarm.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the Alarm.
                deleteAlarm();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the Alarm.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}