/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.pets;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.pets.data.AlarmContract.AlarmEntry;

/**
 * Displays list of alarms that were entered and stored in the app.
 */
public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int ALARM_LOADER = 0;
    AlarmsAdapter mCursorAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup FAB to open EditorActivity
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                startActivity(intent);
            }
        });

        ListView alarmListView = (ListView) findViewById(R.id.alarmsList);

        View emptyView = findViewById(R.id.empty_view);
        alarmListView.setEmptyView(emptyView);

        mCursorAdapter = new AlarmsAdapter(this, null);
        alarmListView.setAdapter(mCursorAdapter);

        alarmListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                Uri CurrentAlarmUri = ContentUris.withAppendedId(AlarmEntry.CONTENT_URI, id);
                intent.setData(CurrentAlarmUri);
                startActivity(intent);
            }
        });
        getLoaderManager().initLoader(ALARM_LOADER, null, this);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_catalog.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_catalog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Insert dummy data" menu option
            case R.id.action_insert_dummy_data:
                insertAlarm();
                return true;
            // Respond to a click on the "Delete all entries" menu option
            case R.id.action_delete_all_entries:
                deleteAllAlarms();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteAllAlarms() {
        int rowsDeleted = getContentResolver().delete(AlarmEntry.CONTENT_URI, null, null);
        Log.v("CatalogActivity", rowsDeleted + " rows deleted from alarm database");
    }


    private void insertAlarm() {
        ContentValues values = new ContentValues();

        values.put(AlarmEntry.COLUMN_ALARM_NAME, "Exam");
        values.put(AlarmEntry.COLUMN_ALARM_DESCRIPTION, "android exam");
        values.put(AlarmEntry.COLUMN_ALARM_ALARM, AlarmEntry.GENDER_ALARM);
        values.put(AlarmEntry.COLUMN_ALARM_TIME, 7);

        Uri newUri = getContentResolver().insert(AlarmEntry.CONTENT_URI, values);
        Log.v("CatalogActivity", "New Row Added Successfully" + newUri);


    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                AlarmEntry._ID,
                AlarmEntry.COLUMN_ALARM_NAME,
                AlarmEntry.COLUMN_ALARM_DESCRIPTION,

        };
        return new CursorLoader(this, AlarmEntry.CONTENT_URI, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }
}
