package com.example.android.pets.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by mohamed on 9/6/2016.
 */
public final class AlarmContract {
    public static final String CONTENT_AUTHORITY = "com.example.android.pets";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_ALARMS = "alarms";

    private AlarmContract() {
    }

    public static final class AlarmEntry implements BaseColumns {


        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_ALARMS);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +
                "/" + CONTENT_AUTHORITY + "/" + PATH_ALARMS;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE +
                "/" + CONTENT_AUTHORITY + "/" + PATH_ALARMS;
        //Table name
        public final static String TABLE_NAME = "alarms";
        // Table Columns
        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_ALARM_NAME = "name";
        public final static String COLUMN_ALARM_DESCRIPTION = "description";
        public final static String COLUMN_ALARM_ALARM = "alarm";
        public final static String COLUMN_ALARM_TIME= "time";

        //Alarm Constants
        public static final int ALARM_UNKNOWN = 0;
        public static final int GENDER_ALARM = 1;
        public static final int GENDER_NO_ALARM = 2;

        public static boolean isValidGender(Integer alarm) {
            if (alarm == ALARM_UNKNOWN || alarm == GENDER_ALARM || alarm == GENDER_NO_ALARM) {
                return true;
            }
            return false;
        }
    }
}
