package com.example.android.pets.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.android.pets.data.AlarmContract.AlarmEntry;

/**
 * Created by mohamed on 9/6/2016.
 */
public class AlarmDBHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = AlarmDBHelper.class.getSimpleName();
    private final static int DATABASE_VERSION = 5;
    private final static String DATABASE_NAME = "tasks.db";


    public AlarmDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String SQL_CREATE_ALARMS_TABLE =  "CREATE TABLE " + AlarmEntry.TABLE_NAME + " ("
                + AlarmEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + AlarmEntry.COLUMN_ALARM_NAME + " TEXT NOT NULL, "
                + AlarmEntry.COLUMN_ALARM_DESCRIPTION + " TEXT, "
                + AlarmEntry.COLUMN_ALARM_ALARM + " INTEGER NOT NULL, "
                + AlarmEntry.COLUMN_ALARM_TIME + " INTEGER NOT NULL DEFAULT 0);";


        sqLiteDatabase.execSQL(SQL_CREATE_ALARMS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
