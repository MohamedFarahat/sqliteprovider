package com.example.android.pets.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.example.android.pets.data.AlarmContract.AlarmEntry;

/**
 * Created by mohamed on 10/3/2016.
 */

public class AlarmProvider extends ContentProvider {
    private AlarmDBHelper mDbHelper;
    public static final String LOG_TAG = AlarmProvider.class.getSimpleName();
    /**
     * URI matcher code for the content URI for the Alarms table
     */
    private static final int ALARMS = 100;
    /**
     * URI matcher code for the content URI for a single Alarm in the Alarms table
     */
    private static final int ALARM_ID = 101;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(AlarmContract.CONTENT_AUTHORITY, AlarmContract.PATH_ALARMS, ALARMS);
        sUriMatcher.addURI(AlarmContract.CONTENT_AUTHORITY, AlarmContract.PATH_ALARMS + "/#", ALARM_ID);
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new AlarmDBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        SQLiteDatabase database = mDbHelper.getReadableDatabase();
        Cursor cursor;

        int match = sUriMatcher.match(uri);
        switch (match) {
            case ALARMS:
                cursor = database.query(AlarmEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case ALARM_ID:
                selection = AlarmEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(AlarmEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    /**
     * Insert new data into the provider with the given ContentValues.
     */
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ALARMS:
                return insertAlarm(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }

    }

    private Uri insertAlarm(Uri uri, ContentValues contentValues) {

        // Check that the name is not null
        String name = contentValues.getAsString(AlarmEntry.COLUMN_ALARM_NAME);
        if (name == null) {
            throw new IllegalArgumentException("Alarm requires a name");
        }

        // Check that the gender is valid
        Integer gender = contentValues.getAsInteger(AlarmEntry.COLUMN_ALARM_ALARM);
        if (gender == null || !AlarmEntry.isValidGender(gender)) {
            throw new IllegalArgumentException("Alarm requires valid Alarm statue ");
        }

        // If the time is provided, check that it's greater than or equal to 0 kg
        Integer time = contentValues.getAsInteger(AlarmEntry.COLUMN_ALARM_TIME);
        if (time != null && time < 0) {
            throw new IllegalArgumentException("Alarm requires valid time");
        }
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        // Insert the new alarm with the given values
        long id = database.insert(AlarmEntry.TABLE_NAME, null, contentValues);
        // If the ID is -1, then the insertion failed. Log an error and return null.
        if (id == -1) {
            Log.e(LOG_TAG, "Failed to insert row for " + uri);
            return null;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        // Return the new URI with the ID (of the newly inserted row) appended at the end
        return ContentUris.withAppendedId(uri, id);
    }


    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ALARMS:
                return updateAlarm(uri, contentValues, selection, selectionArgs);
            case ALARM_ID:
                // For the PET_ID code, extract out the ID from the URI,
                // so we know which row to update. Selection will be "_id=?" and selection
                // arguments will be a String array containing the actual ID.
                selection = AlarmEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                return updateAlarm(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }

    private int updateAlarm(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        // If the {@link AlarmEntry#COLUMN_ALARM_NAME} key is present,
        // check that the name value is not null.
        if (contentValues.containsKey(AlarmEntry.COLUMN_ALARM_NAME)) {
            String name = contentValues.getAsString(AlarmEntry.COLUMN_ALARM_NAME);
            if (name == null) {
                throw new IllegalArgumentException("Alarm requires a name");
            }
        }

        // If the {@link AlarmEntry#COLUMN_Alarm_GENDER} key is present,
        // check that the gender value is valid.
        if (contentValues.containsKey(AlarmEntry.COLUMN_ALARM_ALARM)) {
            Integer gender = contentValues.getAsInteger(AlarmEntry.COLUMN_ALARM_ALARM);
            if (gender == null || !AlarmEntry.isValidGender(gender)) {
                throw new IllegalArgumentException("Alarm requires valid Alarm statue");
            }
        }

        // If the {@link AlarmEntry#COLUMN_Alarm_Time} key is present,
        // check that the time value is valid.
        if (contentValues.containsKey(AlarmEntry.COLUMN_ALARM_TIME)) {
            // Check that the time is greater than or equal to 0 kg
            Integer time = contentValues.getAsInteger(AlarmEntry.COLUMN_ALARM_TIME);
            if (time != null && time < 0) {
                throw new IllegalArgumentException("Alarm requires valid time");
            }
        }
        if (contentValues.size() == 0) {
            return 0;
        }
        // Otherwise, get writeable database to update the data
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

// Perform the update on the database and get the number of rows affected
        int rowsUpdated = database.update(AlarmEntry.TABLE_NAME, contentValues, selection, selectionArgs);
        // If 1 or more rows were updated, then notify all listeners that the data at the
        // given URI has changed
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    /**
     * Delete the data at the given selection and selection arguments.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        int rowsDeleted;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ALARMS:
                // Delete all rows that match the selection and selection args
                rowsDeleted = database.delete(AlarmEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case ALARM_ID:
                // Delete a single row given by the ID in the URI
                selection = AlarmEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = database.delete(AlarmEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    /**
     * Returns the MIME type of data for the content URI.
     */
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ALARMS:
                return AlarmEntry.CONTENT_LIST_TYPE;
            case ALARM_ID:
                return AlarmEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }
}